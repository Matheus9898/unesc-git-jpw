const args = process.argv;

let [pathNode, pathIndex, ...collection] = args;

function isDivisivel2(numero) {
    return numero % 2 == 0; 
}

let divisiveisPorDois = collection.filter(isDivisivel2);

console.log(divisiveisPorDois.reduce((total, numero)=>{
    return parseInt(total) + parseInt(numero);
}, 0));
