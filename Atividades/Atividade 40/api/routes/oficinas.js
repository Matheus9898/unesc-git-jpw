const express = require("express");
const router = express.Router();
const evento = require("../data/evento");

router.get("/", (req, res) => {   
    res.json(evento.getEvento().oficina);
});

router.get("/:id", (req, res) => {

    let oficinasComId = evento.getEvento().oficina.filter(o => o._id == req.params.id);

    if(oficinasComId.length < 1){
        res.json({});
    }else{
        res.json(oficinasComId[0]);
    }
});

router.post("/", (req, res) => {
    const e = evento.getEvento();
    const newOficina = req.body;  

    e.oficina.push(newOficina);
    evento.saveEvento(e);

    res.json(newOficina);
});

router.put('/:id', (req, res) => {
    const e = evento.getEvento();
    const updateOficina = req.body;

    let pos = e.oficina.findIndex( o => o._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        e.oficina[pos] = updateOficina;
        evento.saveEvento(e);
        res.json(updateOficina);
    }
});

router.put('/:id/check-in/:usuarioId', (req, res) => {
    const e = evento.getEvento();

    let posOficina = e.oficina.findIndex( o => o._id == req.params.id);

    if(posOficina < 0){
        res.json({});
    }else{
        const oficina = e.oficina[posOficina];

        let posLocal = e.local.findIndex( l => l._id == oficina.local);
        if(posLocal < 0){
            res.json({});
        }else{
            if(oficina.participantes.length < e.local[posLocal].capacidade){ //Ainda há espaço
                e.oficina[posOficina].participantes.push(parseInt(req.params.usuarioId));
                evento.saveEvento(e);
                res.json(e.oficina[posOficina]);
            }else{
                res.json({erro: {msg: "A capacidade máxima foi atingida", codigo: 400}});
            }
        }
        
    }
});

router.delete('/:id', (req, res) => {
    const e = evento.getEvento();
    let pos = e.oficina.findIndex( o => o._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        const anterior = e.oficina[pos];
        e.oficina.splice(pos, 1);

        evento.saveEvento(e);
        res.json(anterior);
    }
});

router.delete('/:id/check-in/:usuarioId', (req, res) => {
    const e = evento.getEvento();
    let pos = e.oficina.findIndex( o => o._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        const anterior = e.oficina[pos];

        e.oficina[pos].participantes = e.oficina[pos].participantes.filter( p => p != req.params.usuarioId);

        evento.saveEvento(e);
        res.json(anterior);
    }
});

module.exports = router;