var gl = new GeradorLista();

gl.addLista("Shampoo");
gl.addLista("Chocolate");
gl.addLista("Mertiolate");
gl.addLista("Parmalate");

var div = gl.gerarHTML();
for (const item of gl.getLista()) {
    var p = document.createElement('p');
    p.innerHTML = item;
    div.appendChild(p);
    document.body.appendChild(div);
}
