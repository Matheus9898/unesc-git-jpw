var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', function(req, res, next) {
  res.render('login', {msg: null});
});

router.get('/sucesso', function(req, res, next) {
  res.render('sucesso', {usuario: null});
});

router.get('/404', function(req, res, next) {
  res.render('404');
});

router.post('/login', function(req, res, next) {
  let usuario = req.body.usuario;
  let senha = req.body.senha;
  if(usuario === 'root' && senha === 'unesc2019'){
    res.render('sucesso', {usuario});
  } else {
    res.render('login', {msg: 'Credenciais não autorizadas'});
  }
});

module.exports = router;
