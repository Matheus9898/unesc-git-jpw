const express = require("express");
const router = express.Router();
const evento = require("../data/evento");

router.get("/", (req, res) => {   
    res.json(evento.getEvento().oficina);
});

router.get("/:id", (req, res) => {

    let oficinasComId = evento.getEvento().oficina.filter(o => o._id == req.params.id);

    if(oficinasComId.length < 1){
        res.json({});
    }else{
        res.json(oficinasComId[0]);
    }
});

router.post("/", (req, res) => {
    const e = evento.getEvento();
    const newOficina = req.body;  

    e.oficina.push(newOficina);
    evento.saveEvento(e);

    res.json(newOficina);
});

router.put('/:id', (req, res) => {
    const e = evento.getEvento();
    const updateOficina = req.body;

    let pos = e.oficina.findIndex( o => o._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        e.oficina[pos] = updateOficina;
        evento.saveEvento(e);
        res.json(updateOficina);
    }
});

router.delete('/:id', (req, res) => {
    const e = evento.getEvento();
    let pos = e.oficina.findIndex( o => o._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        const anterior = e.oficina[pos];
        e.oficina.splice(pos, 1);

        evento.saveEvento(e);
        res.json(anterior);
    }
});

module.exports = router;