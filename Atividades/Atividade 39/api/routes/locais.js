const express = require("express");
const router = express.Router();
const evento = require("../data/evento");

router.get("/", (req, res) => {   
    res.json(evento.getEvento().local);
});

router.get("/:id", (req, res) => {

    let locaisComId = evento.getEvento().local.filter(l => l._id == req.params.id);

    if(locaisComId.length < 1){
        res.json({});
    }else{
        res.json(locaisComId[0]);
    }
});

router.post("/", (req, res) => {
    const e = evento.getEvento();
    const newLocal = req.body;  

    e.local.push(newLocal);
    evento.saveEvento(e);

    res.json(newLocal);
});

router.put('/:id', (req, res) => {
    const e = evento.getEvento();
    const updateLocal = req.body;

    let pos = e.local.findIndex( l => l._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        e.local[pos] = updateLocal;
        evento.saveEvento(e);
        res.json(updateLocal);
    }
});

router.delete('/:id', (req, res) => {
    const e = evento.getEvento();
    let pos = e.local.findIndex( l => l._id == req.params.id);

    if(pos < 0){
        res.json({});
    }else{
        const anterior = e.local[pos];
        e.local.splice(pos, 1);

        evento.saveEvento(e);
        res.json(anterior);
    }
});

module.exports = router;