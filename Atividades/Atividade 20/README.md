20. Inicie um novo repositório git local e em seguida:
a) Crie um arquivo README.md contendo uma descrição do GIT;
b) Crie uma pasta chamada img e insira uma imagem do git dentro desta pasta;
c) Realize um novo commit com a descrição Estado inicial ;
d) Altere o arquivo README.md e insira a imagem da etapa B dentro do arquivo;
Utilize a sintaxe markdown ![](link) ;
e) Realize um novo commit com a descrição Inserida a imagem do git ;
f) Crie um novo branch a partir do master chamado developer e altere para este
branch;
g) Crie um novo arquivo de texto chamado exe19 contendo as respostas do exercícios
19;
h) Crie um novo commit chamado Incluido Exercicio 19 ;
i) Crie um novo branch em branco chamado exercicios ;
j) Adicione apenas o arquivo exe19 no stash e realize o commit chamado Exercicio
19 ;
h) Crie um novo arquivo chamado AUTHOR.md e insira o seu nome completo;
i) Adicione o arquivo AUTHOR.md no stash e realize o commit com a descrição
Finalizado exercicios ;
j) Realize o merge entre o branch developer e exercicios ;
k) Delete o branch exercicios ;
l) Compacte a pasta do repostório.