var express = require('express');
var router = express.Router();
const fs = require('fs');
const level = require('level');
const db = level('UNESC');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/save', function(req, res, next) {
  let conteudo = null;
  let title = 'Arquivo salvo com sucesso!';
  try {
    conteudo = fs.readFileSync(req.body.pathFile, 'utf8');
  } catch (error) {}

  if (conteudo != null) {
    db.put('conteudo', conteudo, function(err){
      if (err) {
        title = 'Não foi possível salvar no banco!';
      }
    });
  }

  res.render('index', { title: title });
});


module.exports = router;
